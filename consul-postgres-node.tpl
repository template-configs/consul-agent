{{ with $secrets := secret "config/postgres_node" }}
server = false
datacenter = "{{$secrets.Data.data.consul_datacenter}}"
node_name = "{{$secrets.Data.data.consul_node_name}}"
data_dir = "/opt/consul"
encrypt = "{{$secrets.Data.data.consul_encrypt}}"
leave_on_terminate = true
enable_local_script_checks = true
disable_remote_exec = false
retry_join = [
        "{{$secrets.Data.data.consul_server_addr}}"
    ]

acl = {
  enabled = true
  default_policy = "deny"
  down_policy = "extend-cache"
  tokens = {
      default = "{{$secrets.Data.data.consul_default_token}}"
    }
  }


services {
  id = "{{$secrets.Data.data.consul_service_id}}"
  name = "{{$secrets.Data.data.consul_service_name}}"
  tags = [
    "leader",
    "postgres"
  ]
  address = ""
  port = {{$secrets.Data.data.consul_service_port}}
  checks = [
    {
      id = "{{$secrets.Data.data.consul_service_check_id}}"
      tcp = "{{$secrets.Data.data.consul_service_check_tcp}}"
      interval = "10s"
      timeout = "20s"
    }
  ]
}
{{ end }}
