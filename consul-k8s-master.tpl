{{ with $secrets := secret "config/k8s_master" }}
server = false
datacenter = "{{$secrets.Data.data.consul_datacenter}}"
data_dir = "/opt/consul"
encrypt = "{{$secrets.Data.data.consul_encrypt}}"
leave_on_terminate = true
enable_local_script_checks = true
disable_remote_exec = false
retry_join = [
        "{{$secrets.Data.data.consul_server_addr}}"
    ]

acl = {
  enabled = true
  default_policy = "deny"
  down_policy = "extend-cache"
  tokens = {
      default = "{{$secrets.Data.data.consul_default_token}}"
    }
  }

services {
  id = "{{$secrets.Data.data.consul_service_id}}"
  name = "{{$secrets.Data.data.consul_service_name}}"
  tags = [
    "leader",
    "kubernetes"
  ]
  port = {{$secrets.Data.data.consul_service_port}}
  checks = [
    {
      id = "{{$secrets.Data.data.consul_service_check_id}}"
      http = "{{$secrets.Data.data.consul_service_check_http}}"
      tls_skip_verify = true
      interval = "10s"
      timeout = "2s"
    }
  ]
}
{{ end }}
